package authdbconnector;

import com.mysql.jdbc.Driver;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class DBConnector {

    private static boolean init = false;
    private static final int totalTry = 10;
    private static final long connectionHoldTime = 1800000L;
    private static final String configFile = "AuthDBConfig.txt";
    private static Properties properties = null;
    private static List<DBConnection> free = null;
    private static List<DBConnection> used = null;
    private static DBConnector dbConnector = null;
    private static final Logger logger = Logger.getLogger(DBConnector.class.getName());

    private DBConnector() {
        free = new ArrayList<>();
        used = new ArrayList<>();
    }

    public static DBConnector getInstance() throws Exception {
        if (!init) {
            createDBConnector();
        }
        return dbConnector;
    }

    private static synchronized void createDBConnector() throws Exception {
        int failCount = 0;
        dbConnector = new DBConnector();
        while (true) {
            /* run driverTest method */
            if (!dbConnector.driverTest()) {
                return;
            }
            DriverManager.registerDriver((Driver) Class.forName("com.mysql.jdbc.Driver").newInstance());
            /* read database conifurations */
            try {
                InputStream input;
                File file = new File(configFile);
                if (file.exists()) {
                    input = new FileInputStream(file);
                } else {
                    input = Thread.currentThread().getContextClassLoader().getResourceAsStream(configFile);
                }
                if (input != null) {
                    properties = new Properties();
                    properties.load(input);
                    input.close();
                    if (!properties.containsKey("host")) {
                        logger.error("No host is found in DBConfig.txt");
                        return;
                    }
                    if (!properties.containsKey("database")) {
                        logger.error("No database is found in DBConfig.txt");
                        return;
                    }
                    if (!properties.containsKey("user")) {
                        logger.error("No user is found in DBConfig.txt");
                        return;
                    }
                    if (!properties.containsKey("password")) {
                        logger.error("No password is found in DBConfig.txt");
                        return;
                    }
                    if (!properties.containsKey("useUnicode")) {
                        logger.info("No value found for useUnicode in DBConfig.txt");
                    }
                    if (!properties.containsKey("characterEncoding")) {
                        logger.info("No value found for characterEncoding in DBConfig.txt");
                    }
                    if (!properties.containsKey("connectTimeout")) {
                        logger.info("No value found for connectTimeout in DBConfig.txt");
                    }
                    if (!properties.containsKey("socketTimeout")) {
                        logger.info("No value found for socketTimeout in DBConfig.txt");
                    }
                }
            } catch (IOException e) {
                logger.fatal("Exception in reading DB config file:" + e);
            }

            /* make the connection to the database */
            try {
                DBConnection dbConnection = dbConnector.makeConnection();
                used.remove(dbConnection);
                free.add(dbConnection);
                init = true;
                break;
            } catch (Exception ex) {
                failCount++;
                if (failCount >= totalTry) {
                    break;
                }
                logger.fatal("Exception in making DB connection:" + ex);
                try {
                    Thread.currentThread().sleep(failCount * 1000);
                } catch (InterruptedException interruptException) {
                }
            }
        }
    }

    private boolean driverTest() {
        boolean success = true;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            logger.debug("MySQL Driver is Found");
        } catch (java.lang.ClassNotFoundException e) {
            success = false;
            logger.fatal("MySQL JDBC Driver is not found ... " + e);
        }
        return success;
    }

    public synchronized DBConnection makeConnection() throws Exception {
        DBConnection dbConnection = null;
        try {
            while (!free.isEmpty()) {
                dbConnection = free.remove(0);
                if (dbConnection == null || dbConnection.connection == null || dbConnection.connection.isClosed()) {
                    continue;
                }
                boolean isValid;
                try {
                    isValid = dbConnection.connection.isValid(5);
                } catch (SQLException sqlEx) {
                    isValid = false;
                    logger.info("DatabaseConnection is invalid " + dbConnection.createdTime, sqlEx);
                }
                if ((dbConnection.createdTime + connectionHoldTime) < System.currentTimeMillis() || !isValid) {
                    try {
                        dbConnection.connection.close();
                        logger.info("DatabaseConnection has been CLOSED(" + isValid + ") for ConnectionID " + dbConnection.createdTime);
                    } catch (SQLException th) {
                        logger.fatal("Failed to close connection", th);
                    }
                    continue;
                }

                used.add(dbConnection);
                return dbConnection;
            }

            dbConnection = new DBConnection();
            dbConnection.createConnection(properties);
            used.add(dbConnection);

            logger.info("DatabaseConnection NEW ConnectionID " + dbConnection.createdTime);
            logger.info("DatabaseConnections used " + used.size() + ", free " + free.size());
        } catch (Exception e) {
            logger.fatal(e.getMessage(), e);
            throw e;
        }
        return dbConnection;
    }

    public synchronized void freeConnection(DBConnection dbConnection) {
        if (used.contains(dbConnection)) {
            used.remove(dbConnection);
        }
        if (!free.contains(dbConnection)) {
            free.add(dbConnection);
        }
    }

    public void closeAllConnections() {
        int counter = 0;
        while (!used.isEmpty()) {
            counter++;
            try {
                if (counter > 7) {
                    while (used.size() > 0) {
                        DBConnection dbConnection = used.get(0);
                        if (dbConnection != null) {
                            logger.info("Till now using connection created at " + new java.util.Date(dbConnection.createdTime));
                            freeConnection(dbConnection);
                        }
                    }
                    break;
                } else {
                    logger.info("Waiting for making connections free ...");
                    Thread.sleep(100);
                }
            } catch (InterruptedException ex) {
                logger.error("InterruptedException in closeAllConnections ", ex);
            }
        }
        if (used.isEmpty()) {
            logger.info("DATABASE no more used connection !!!! All connections are free");
        }

        logger.info("DATABASE free connection list size " + free.size());
        while (!free.isEmpty()) {
            DBConnection dbConnection = free.remove(0);
            try {
                logger.info("DATABASE free connection list size " + free.size());
                if (dbConnection != null) {
                    if (dbConnection.connection != null && !dbConnection.connection.isClosed()) {
                        dbConnection.connection.close();
                    }
                }
            } catch (SQLException e) {
                logger.fatal(e.getMessage(), e);
            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            closeAllConnections();
        } finally {
            super.finalize();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        try {
            PropertyConfigurator.configure("log4j.properties");
            DBConnection d = DBConnector.getInstance().makeConnection();
            d = DBConnector.getInstance().makeConnection();
            d = DBConnector.getInstance().makeConnection();
            d = DBConnector.getInstance().makeConnection();
            d = DBConnector.getInstance().makeConnection();
            d = DBConnector.getInstance().makeConnection();
            DBConnector.getInstance().closeAllConnections();
        } catch (Exception ex) {
        }
    }
}

package scdbconnector;

import java.sql.*;
import java.util.Properties;
import org.apache.log4j.Logger;

public class DBConnection {

    private static final Logger logger = Logger.getLogger(DBConnection.class.getName());
    public Connection connection = null;
    public long createdTime = 0;
//    private Properties properties;

    public DBConnection() {
        createdTime = System.currentTimeMillis();
    }

    public Connection getConnection() {
        return connection;
    }

    public void createConnection(Properties properties) throws Exception {
        String url = "";
        try {
            connection = DriverManager.getConnection("jdbc:mysql:loadbalance://" + properties.getProperty("host") + "/" + properties.getProperty("database"), properties);
        } catch (SQLException e) {
            logger.fatal("Connection couldn't be established to " + url);
            throw (e);
        }
    }

    public static void main(String[] args) {
        DBConnection dbCon = new DBConnection();
        try {
            Properties prop = new Properties();
            prop.setProperty("host", "localhost");
            prop.setProperty("database", "ringauth");
            prop.setProperty("user", "root");
            prop.setProperty("password", "abc123");
            prop.setProperty("useUnicode", "true");
            prop.setProperty("socketTimeout", "15");
            prop.setProperty("characterEncoding", "UTF-8");
            prop.setProperty("useConfigs", "clusterBase");

            dbCon.createConnection(prop);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
